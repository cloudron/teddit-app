FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

ARG VERSION=0.4.0

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

RUN curl -L https://codeberg.org/teddit/teddit/archive/${VERSION}.tar.gz | tar -xvzf - --strip-components 1

RUN npm install --no-optional
RUN ln -s /app/data/config.js /app/code/config.js
RUN mv /app/code/static /app/code/static.original && ln -s /run/teddit/static /app/code/static

COPY config.js start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
