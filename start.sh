#!/bin/bash

set -eu

mkdir -p /run/teddit/static

if [[ ! -f /app/data/config.js ]]; then
    echo "==> Copying files on first run"
    # https://codeberg.org/teddit/teddit/src/branch/main/config.js.template
    cp /app/pkg/config.js /app/data/config.js
fi

rsync -avz /app/code/static.original/* /run/teddit/static/

chown -R cloudron:cloudron /run/teddit /app/data

echo "==> Starting Teddit"
exec /usr/local/bin/gosu cloudron:cloudron node app.js
