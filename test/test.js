#!/usr/bin/env node

'use strict';

/* jshint esversion: 8 */
/* global describe */
/* global before */
/* global after */
/* global it */
/* global xit */

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome'),
    assert = require('assert');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME, PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    let LOCATION = 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;

    let browser, app;
    let username = process.env.USERNAME;
    let password = process.env.PASSWORD;

    before(function () {
        const options = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.HEADLESS) options.addArguments('headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(options).build();
    });

    after(function () {
        browser.quit();
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(username, password) {
        await browser.get(`https://${app.fqdn}/login`);

        await browser.wait(until.elementLocated(By.xpath('//input[@name="username"]')));
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.findElement(By.id('login')).click();
        await browser.wait(until.elementLocated(By.id('search')));
        await browser.sleep(2000);
    }

    async function getMainPage() {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.id('search')));
        await browser.sleep(2000);
    }


    async function searchContent() {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.id('search')));

        await browser.findElement(By.xpath('//div[@id="search"]//input[@id="q"]')).sendKeys("Fast boats");
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//div[@id="search"]//input[@value="search"]')).click();
        await browser.sleep(3000);
        await waitForElement(By.xpath('//div[@class="link"]//div[@class="entry"]'));
        await browser.sleep(3000);
    }

    async function logout() {
        await browser.get('https://' + app.fqdn + '/logout');
        await browser.sleep(3000);
        await browser.manage().deleteAllCookies();
        await waitForElement(By.xpath('//input[@name="username"]'));
        await browser.sleep(3000);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login', login.bind(null, username, password));
    it('can get the main page', getMainPage);
    it('can search', searchContent);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can login', login.bind(null, username, password));
    it('can search', searchContent);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login.bind(null, username, password));
    it('can get the main page', getMainPage);
    it('can search', searchContent);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can login', login.bind(null, username, password));
    it('can get the main page', getMainPage);
    it('can search', searchContent);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('install app for update', function () { execSync(`cloudron install --appstore-id com.teddit.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, username, password));
    it('can get the main page', getMainPage);
    it('can search', searchContent);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can login', login.bind(null, username, password));
    it('can get the main page', getMainPage);
    it('can search', searchContent);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
